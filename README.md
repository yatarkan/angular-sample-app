# AngularSampleApp

This is sample Angular 8 single-page application, generated with [Angular CLI](https://github.com/angular/angular-cli).

## Prerequisites

Before you begin, make sure your development environment includes Node.js® and an npm package manager.

#### Node.js
Angular requires Node.js version 10.9.0 or later.

 - To check your version, run `node -v` in a terminal/console window.

 - To get Node.js, go to [nodejs.org](https://nodejs.org/).

## Installation

To install dependencies run `npm install`

## Run Development server

Run `npm start` or `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
